var axios = require('axios');

const assert = require('assert');
const chai = require('chai')
var expect = chai.expect;

const redsky = require('../comm/redsky.js')

describe('testing redsky module interface',
  function() {
    it('product name is returned if item is found', function * () {
      const axiosStub = this.sandbox.stub(axios, 'get').callsFake(function (url) {
        return new Promise(function (resolve, reject) {
          let happyPathResponse = {
            data: { product: { item: {product_description: {title: 'Dead Poets Society'}}}}
          }
          resolve(happyPathResponse)
        })
      })

      const result = yield redsky.getItemName('1234')

      expect(axiosStub).to.be.calledWith('https://redsky.target.com/v2/pdp/tcin/1234?excludes=product_vendors,taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics')
      expect(result).to.eql('Dead Poets Society')
    })

    it('rejects promise if item is not found in response', function * () {
      const axiosStub = this.sandbox.stub(axios, 'get').callsFake(function (url) {
        return new Promise(function (resolve, reject) {
          let happyPathResponse = {
            data: { product: { item: {}}}
          }
          resolve(happyPathResponse)
        })
      })

      return redsky.getItemName('000').then(function fulfilled(result) {
        throw new Error('promise was unexpectedly fulfilled')
      }).catch(function(error) {
        assert(error == 'item not found')
      })

    })

    it('rejects promise if a http error is encountered', function * () {
      const axiosStub = this.sandbox.stub(axios, 'get').callsFake(function (url) {
        return new Promise(function (resolve, reject) {
          return reject('Error: Request failed with status code 404')
        })
      })

      return redsky.getItemName('000').then(function fulfilled(result) {
        throw new Error('promise was unexpectedly fulfilled')
      }).catch(function(error) {
        assert(error == 'Error: Request failed with status code 404')
      })
    })
  }
)