# myRetail Client API

The myRetail Client API is a RESTful web service built to service the needs of iternal and external myRetail customers.  It makes internal product data available for any number of clients to consume.  A live instance of the app is hosted at https://chrisg-my-retail.herokuapp.com/.

## Getting Started

To run locally start `mongod`, then clone the repository and import the default dataset using:

```
mongoimport --db my_retail --collection prices --file integration-test/prices.json
```

Next, run `npm install` & `npm start` from the locally cloned repo directory to run a copy of the server locally.  

If you have docker installed and just want to run the app, you can also run the latest pre-release build from develop by running:

```
$ docker run -p 49160:3000 -d  registry.gitlab.com/chrisg-case-study/myretail
```

This will point to the cloud instance mongodb backend, and so running mongod locally will not be necessary.

## Prerequisites

node.js 9.0+
docker 18.0+
mongodb 4.0+

## Using the API

See [the swagger docs](https://chrisg-my-retail.herokuapp.com/api-docs/) for usage. 

Note: You may have to enable an insecure connection to the site in your browser for the swagger docs to work properly in the hosted environment.  Running the application locally and visiting localhost:3000/api-docs/ works fine.

## Running in Docker Container

If you have docker installed, you can run the app in a local docker container, which is true to the deployment environment.  A Dockerfile is included in the repo, so you should be able to just build a docker image using:

```
$ docker build -t <your username>/<your image name> .
```

and then run it using:

```
$ docker run -p 49160:8080 -d <your username>/<your image name>
```

For more information, see [Dockerizing a Node.js web app](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/).

## Running the Tests

This app uses unit tests to check functionality at the low lever, and integration tests to check for regressions at the API level.  To run the entire test suite, run `npm run test`.  To run unit or integration tests separately, you can run `npm run test-unit` or `npm run test-integration`.

## Deployment

Merges to Master have their docker containers automatically deployed to heroku staging.  Someone with the proper credentials just needs to run:

```
$ heroku container:release web -a chrisg-my-retail
```

to make it go live at: https://chrisg-my-retail.herokuapp.com/

Merges to develop are automatically added to the docker registry for this project in gitlab, and can be run locally:

```
$ docker run -p 49160:3000 -d  registry.gitlab.com/chrisg-case-study/myretail
```

## Contributing

Just make a branch off of develop and make a pull request with your changes back to develop when ready.  Be ready to answer any questions and make changes as needed after review.  

