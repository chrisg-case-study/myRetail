 express = require('express');
var router = express.Router();
var redsky = require('../comm/redsky.js');

/* GET products listing. */
router.get('/:id', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  let product_id_string = req.params.id
  let product_id_number = parseInt(req.params.id)
  let prices = req.app.get('db').collection('prices')
  prices.findOne({product_id: product_id_number}).then(item => {
    redsky.getItemName(product_id_string).then(itemName => {
      
      let response = {
        id: product_id_number,
        name: itemName,
        current_price: item.current_price
      }
      res.send(response)
    }).catch(error => {
      if (error == 'Error: Request failed with status code 404') {
        return res.status(404).send({error: 'Product ID not found'})  
      } else {
        console.log('an error occurred: ' + error)
        return res.status(500).send({error: 'An error occurred'})
      }
        
    })
  })
});

/* PUT product price. */
router.put('/:id', function(req, res, next) {
  //TODO: figure out why adding this to app.use() in app.js did not work.
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  let product_id_string = req.params.id
  let product_id_number = parseInt(req.params.id)
  let new_price_value = req.body.current_price.value
  if (isNaN(new_price_value)) {
    return res.status(400).send({error: 'price value must be a number'})
  }
  redsky.getItemName(product_id_string).then(itemName => {
    let prices = req.app.get('db').collection('prices')
    prices.updateOne({product_id: product_id_number}, {$set: {"current_price.value": new_price_value}}, function(error, result) {
      if (error) {
        return res.status(500).send({error: 'An error occurred'})
      }
      var response = {
        message: 'item price updated',
        item: req.body
      }
      res.send(response)
    })
  }).catch(error => {
    if (error == 'Error: Request failed with status code 404') {
      res.status(404).send({error: 'Product ID not found'})
    } else {
      res.status(500).send({error: 'An error occurred'})
    }
    console.log('an error occurred: ' + error)
  })
})

module.exports = router;
