var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var indexRouter = require('./routes/index');
var productsRouter = require('./routes/products');
var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./api_doc/swagger.json');
var app = express();
var database = require('./dbs');
var db_uri = process.env.MONGODB_URI ? process.env.MONGODB_URI : 'mongodb://localhost:27017/my_retail'

function initializeDatabase() {
  return new Promise(function (resolve, reject) {
    console.log('connecting to database at: ' + db_uri)
    database.connect(db_uri).then(db => {
      //shared db connection
      app.set('db', db);
      resolve(app)   
    }).catch(err => {
      return reject(err)
    })
  })

}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//logger
app.use(logger('dev'));

//encoding/decoding
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//setup routes
var v1 = express.Router();
v1.use('/', indexRouter);
v1.use('/products', productsRouter);
v1.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/', v1);
app.use('/v1', v1);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//connect to mongodb
initializeDatabase().then(app => {
  app.emit('ready')
}).catch(err => {
  console.error('Failed to make database connection!')
  console.error(err)
  process.exit(1)
})

module.exports = app
