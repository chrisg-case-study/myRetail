var axios = require('axios');
var _ = require('lodash');

function getItemName(id) {
    return new Promise( function (resolve, reject) {
        let baseRedskyUrl = 'https://redsky.target.com/v2/pdp/tcin/'
        let redskyQueryParameter = '?excludes=product_vendors,taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics'
        let redskyQueryUrl = baseRedskyUrl + id + redskyQueryParameter
        axios.get(redskyQueryUrl).then(redskyResponse => {
            let itemName = _.get(redskyResponse, ['data', 'product', 'item', 'product_description', 'title'], undefined) 
            if (typeof itemName == 'undefined') {
                return reject('item not found')
            }
            resolve(itemName)
        }).catch(error => {
            return reject(error)
        })
    })
}

module.exports = {
    getItemName
}