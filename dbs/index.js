const MongoClient = require('mongodb').MongoClient
 
// Note: A production application should not expose database credentials in plain text.
// For strategies on handling credentials, visit 12factor: https://12factor.net/config.

 
function connect(db_uri) {
  return new Promise( function(resolve, reject) {
    MongoClient.connect(db_uri, function (err, database) {
      if (err) {
        return reject(err)
      }
      resolve(database.db('my_retail'))
    })
  })
}
 
module.exports = {
  connect
}