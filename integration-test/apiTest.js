const request = require('supertest')
const app = require('../app')
const agent = request.agent(app);

before(function (done) {
    app.on('ready', function(){
        done();
    });
});

/**
 * Testing for /products endpoint
 */

describe('GET /products/id', function () {
    it('responds with product details for The Big Lebowski', function (done) {
        agent
            .get('/products/13860428')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, {
                id:13860428,
                name:"The Big Lebowski (Blu-ray)",
                current_price: {
                    value:19.99,
                    currency_code:"USD"
                }
            }, done)
    })

    it('responds with the proper product item details for SpongeBob Frozen Face-off', function (done) {
        agent
            .get('/products/13860429')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, {
                id:13860429,
                name:"SpongeBob SquarePants: SpongeBob's Frozen Face-off",
                current_price: {
                    value:4.99,
                    currency_code:"USD"
                }
            }, done)
    })

    it('responds with the proper product item details for Godzilla', function (done) {
        agent
            .get('/products/13860425')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, {
                id:13860425,
                name:"Godzilla (Blu-ray)",
                current_price: {
                    value:9.99,
                    currency_code:"USD"
                }
            }, done)
    })

    it('lets the client know when a product ID is not found', function (done) {
        agent
            .get('/products/0000000')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404, {
                error: 'Product ID not found'
                }, done)
    })
})

describe('PUT /products/id', function () {
    it('returns error if invalid price passed in', function (done) {
        agent
            .put('/products/13860428')
            .set('Accept', 'application/json')
            .send({
                id:13860428,
                name:"The Big Lebowski (Blu-ray)",
                current_price: {
                    value:"abc",
                    currency_code:"USD"
                }
            })
            .expect('Content-Type', /json/)
            .expect(400, {
                error: 'price value must be a number'
            }, done)
    })

})